name = "Philippe Tan"
age = 23
occupation = "student"
movie = "Star Wars"
rating = 95.5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%.")

num1 = 10
num2 = 20
num3 = 30

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)